const devUrl = 'https://admin.xinhuayuan.com.cn/admin/'
const serverUrl = 'https://admin.xinhuayuan.com.cn/admin/'
const frontURL = process.env.NODE_ENV === 'development' ? 'https://api.xinhuayuan.com.cn/' : 'https://api.xinhuayuan.com.cn/'
const baseURL = process.env.NODE_ENV === 'development' ? devUrl : serverUrl
export {frontURL, baseURL}
